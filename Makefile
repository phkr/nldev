# nldev - NetLink Device manager 
# See LICENSE file for copyright and license details.

include config.mk

SRC = ${NAME}.c
OBJ = ${SRC:.c=.o}

all: options ${NAME}

options:
	@echo ${NAME} build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	@echo CC $<
	@${CC} -c ${CFLAGS} $<

${OBJ}: config.mk

${NAME}: ${OBJ}
	@echo CC -o $@
	@${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	@echo cleaning
	@rm -f ${NAME} ${OBJ} ${NAME}-${VERSION}.tar.gz

dist: clean
	@echo creating dist tarball
	@mkdir -p ${NAME}-${VERSION}
	@cp -R LICENSE Makefile README.md FIXES.md config.mk \
		${SRC} ${NAME}.8 *.h ${NAME}-${VERSION}
	@tar -cf ${NAME}-${VERSION}.tar ${NAME}-${VERSION}
	@gzip ${NAME}-${VERSION}.tar
	@rm -rf ${NAME}-${VERSION}

install: all
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f ${NAME} ${DESTDIR}${PREFIX}/bin
	@cp -f run_${NAME} ${DESTDIR}${PREFIX}/bin
	@cp -f nltrigger ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/${NAME}
	@echo installing manual page to ${DESTDIR}${MANPREFIX}/man8
	@mkdir -p ${DESTDIR}${MANPREFIX}/man8
	@cp -f ${NAME}.8 ${DESTDIR}${MANPREFIX}/man8
	@chmod 644 ${DESTDIR}${MANPREFIX}/man8/${NAME}.8

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/${NAME}
	@rm -f ${DESTDIR}${PREFIX}/bin/run_${NAME}
	@rm -f ${DESTDIR}${PREFIX}/bin/nltrigger
	@echo removing manual page from ${DESTDIR}${PREFIX}/man8
	@rm -f ${DESTDIR}${MANPREFIX}/man8/${NAME}.8

.PHONY: all options clean dist install uninstall
