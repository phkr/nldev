# NetLink DEVice manager

## Installation

	# edit config.mk according to your needs
	% make
	% make PREFIX=/ MANPREFIX=/usr/share/man install

## nldev + mdev 

For getting nldev and mdev to work use the following instructions.

	% cd /tmp
	% git clone git://busybox.net/busybox.git
	% cd busybox
	% make
	% cp busybox /bin
	% cd /bin
	% ln -s busybox mdev

Now copy over all the required files.

	% cd $nldevdir
	% mkdir -p /lib/mdev
	% mkdir -p /etc
	% cp mdev/etc/mdev.conf /etc
	% cp mdev/lib/* /lib/mdev 

In you init scripts you will need to trigger:

	% run_nldev &
	% nltrigger all

By default *run_nldev* will log to the *daemon* facility and the
notice level. This can be adjusted by editing *run_nldev* direct-
ly.

### CAUTION

Because of the asynchronous initialisation of every device that
needs some firmware or some that just take some time to appear
in the kernel, don't assume interfaces to be there after startup.
Always add some step to check for the devices to be there or do
them per service.

## udev replacement

Many application directly depend on libudev and so need the
overengineered backends of udev to work. Most of this (like X11)
can be avoided by having symlinks to static devices and leaving
all the automatism in one place instead of many.

Scripts that call the udev tools directly will need to be patched,
like laptop-mode-utils.

## advanced nldev

As shown in the manpages does nldev allow some more flexibility,
but was mainly written to be a missing piece in replacing udevd.

For example many instances of nldev could be run, call scripts
with simpler logic than mdev and doing less.

## nlmon

On http://git.r-36.net/nlmon/ you will find the little brother
of nldev, which can be run without root rights as user and used
to run scripts on certain events.


Have fun!

