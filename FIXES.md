## Fixes in various tools depending on udev

### laptop-mode-tools

Either remove udevd completely, which is impossible on some
setups right now (04/12) or do the following:

	% mv /usr/bin/udevadm /usr/bin/_udevadm
	% mv /sbin/udevadm /sbin/_udevadm

This will prevent laptop-mode-tools from finding those tools
and not calling udevadm on this, which will of course fail,
because no udevd is running.

